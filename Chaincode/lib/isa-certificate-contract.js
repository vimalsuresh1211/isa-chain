/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class IsaCertificateContract extends Contract {



    //FIRE DEPARTMENT
    async fireCertificateExists(ctx, fireCertificateId) {
        const buffer = await ctx.stub.getState(fireCertificateId);
        return (!!buffer && buffer.length > 0);
    }

    async createfireCertificate(ctx, fireCertificateId, value) {
        const exists = await this.fireCertificateExists(ctx, fireCertificateId);
        if (exists) {
            throw new Error(`The Fire Department ${fireCertificateId} already exists`);
        }
        const asset = { value };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(fireCertificateId, buffer);
    }

    async readFireCertificate(ctx, fireCertificateId) {
        const exists = await this.fireCertificateExists(ctx, fireCertificateId);
        if (!exists) {
            throw new Error(`The Fire Department certificate Number ${fireCertificateId} does not exist`);
        }
        const buffer = await ctx.stub.getState(fireCertificateId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateFireCertificate(ctx, fireCertificateId, newValue) {
        const exists = await this.fireCertificateExists(ctx, fireCertificateId);
        if (!exists) {
            throw new Error(`The Fire Department certificate ${fireCertificateId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(fireCertificateId, buffer);
    }

    async deleteFireCertificate(ctx, fireCertificateId) {
        const exists = await this.fireCertificateExists(ctx, fireCertificateId);
        if (!exists) {
            throw new Error(`The Fire Department certificate ${fireCertificateId} does not exist`);
        }
        await ctx.stub.deleteState(fireCertificateId);
    }




    //ELECTRICITY DEPARTMENT
    async electricityCertificateExists(ctx, electrictyCertificateId) {
        const buffer = await ctx.stub.getState(electrictyCertificateId);
        return (!!buffer && buffer.length > 0);
    }

    async createElectricityCertificate(ctx,fireCertificateId, electrictyCertificateId, value) {
        const fireCertificate = await this.fireCertificateExists(ctx, fireCertificateId);
        if (!fireCertificate) {
            throw new Error(`The Fire Department Approval ${fireCertificateId} does not exists`);
        }
        const electricityCertificate = await this.electricityCertificateExists(ctx,electrictyCertificateId);
        if (electricityCertificate) {
            throw new Error(`The Electricity Department Certificate Number ${electrictyCertificateId} already exists`);
        }

        const asset = { value };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(electricityCertificateId, buffer);
    }

    async readelectricityCertificate(ctx, electricityCertificateId) {
        const exists = await this.electricityCertificateExists(ctx,electricityCertificateId);
        if (!exists) {
            throw new Error(`The Electricity Department certificate Number ${electricityCertificateId} does not exist`);
        }
        const buffer = await ctx.stub.getState(electricityCertificateId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateFireCertificate(ctx,electricityCertificateId, newValue) {
        const exists = await this.electricityCertificateExists(ctx,electricityCertificateId);
        if (!exists) {
            throw new Error(`The Electricity Department certificate ${electricityCertificateExists} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(electricityCertificateId, buffer);
    }

    async deleteFireCertificate(ctx,electricityCertificateId) {
        const exists = await this.electricityCertificateExists(ctx, electricityCertificateId);
        if (!exists) {
            throw new Error(`The Electricity Department certificate ${electricityCertificateId} does not exist`);
        }
        await ctx.stub.deleteState(electricityCertificateId);
    }



    //CORPORATION DEPARTMENT
    async tcCertificateExists(ctx, tcCertificateId) {
        const buffer = await ctx.stub.getState(tcCertificateId);
        return (!!buffer && buffer.length > 0);
    }

    async createTcCertificate(ctx,fireCertificateId, electrictyCertificateId,tcCertificateId, value) {
        const firecertificate = await this.fireCertificateExists(ctx, fireCertificateId);
        if (firecertificateexists) {
            throw new Error(`The Fire Department Approval ${fireCertificateId} does not exists`);
        }
        const electricitycertificate = await this.electricityCertificateExists(ctx,electrictyCertificateId);
        if (!electricitycertificate) {
            throw new Error(`The Electricity Department Approval ${electrictyCertificateId} does not exists`);
        }
        const tcCertificate = await this.tcCertificateExists(ctx,tcCertificateId);
        if (tcCertificate) {
            throw new Error(`The Corporation Approval ${tcCertificateId} already exists`);
        }


        const asset = { value };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(tcCertificateId, buffer);
    }

    async readTcCertificate(ctx, tcCertificateId) {
        const exists = await this.tcCertificateExists(ctx,tcCertificateId);
        if (!exists) {
            throw new Error(`The Corporation certificate Number ${tcCertificateId} does not exist`);
        }
        const buffer = await ctx.stub.getState(tcCertificateId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateTcCertificate(ctx,tcCertificateId, newValue) {
        const exists = await this.tcCertificateExists(ctx,tcCertificateId);
        if (!exists) {
            throw new Error(`The Tc certificate ${tcCertificateId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(tcCertificateId, buffer);
    }

    async deleteTcCertificate(ctx,tcCertificateId) {
        const exists = await this.tcCertificateExists(ctx, electricityCertificateId);
        if (!exists) {
            throw new Error(`The Corporation Department certificate ${electricityCertificateId} does not exist`);
        }
        await ctx.stub.deleteState(electricityCertificateId);
    }

}

module.exports = IsaCertificateContract;
