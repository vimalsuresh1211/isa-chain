/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const IsaCertificateContract = require('./lib/isa-certificate-contract');

module.exports.IsaCertificateContract = IsaCertificateContract;
module.exports.contracts = [ IsaCertificateContract ];
