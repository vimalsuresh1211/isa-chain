/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { IsaCertificateContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logging = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('IsaCertificateContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new IsaCertificateContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"isa certificate 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"isa certificate 1002 value"}'));
    });

    describe('#isaCertificateExists', () => {

        it('should return true for a isa certificate', async () => {
            await contract.isaCertificateExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a isa certificate that does not exist', async () => {
            await contract.isaCertificateExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createIsaCertificate', () => {

        it('should create a isa certificate', async () => {
            await contract.createIsaCertificate(ctx, '1003', 'isa certificate 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"isa certificate 1003 value"}'));
        });

        it('should throw an error for a isa certificate that already exists', async () => {
            await contract.createIsaCertificate(ctx, '1001', 'myvalue').should.be.rejectedWith(/The isa certificate 1001 already exists/);
        });

    });

    describe('#readIsaCertificate', () => {

        it('should return a isa certificate', async () => {
            await contract.readIsaCertificate(ctx, '1001').should.eventually.deep.equal({ value: 'isa certificate 1001 value' });
        });

        it('should throw an error for a isa certificate that does not exist', async () => {
            await contract.readIsaCertificate(ctx, '1003').should.be.rejectedWith(/The isa certificate 1003 does not exist/);
        });

    });

    describe('#updateIsaCertificate', () => {

        it('should update a isa certificate', async () => {
            await contract.updateIsaCertificate(ctx, '1001', 'isa certificate 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"isa certificate 1001 new value"}'));
        });

        it('should throw an error for a isa certificate that does not exist', async () => {
            await contract.updateIsaCertificate(ctx, '1003', 'isa certificate 1003 new value').should.be.rejectedWith(/The isa certificate 1003 does not exist/);
        });

    });

    describe('#deleteIsaCertificate', () => {

        it('should delete a isa certificate', async () => {
            await contract.deleteIsaCertificate(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a isa certificate that does not exist', async () => {
            await contract.deleteIsaCertificate(ctx, '1003').should.be.rejectedWith(/The isa certificate 1003 does not exist/);
        });

    });

});